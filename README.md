# Final Lo Visto

Práctica final del curso 2020/21

# Entrega practica
## Datos
* Nombre: Irene Robledo Calero
* Titulación: Ingeniería en Sistemas de Telecomunicación
* Video básico (url): https://youtu.be/sSYAwPclulI
* Video parte opcional (url): https://youtu.be/tzp8rf0uOOI
* Despliegue (url): http://irobledo.pythonanywhere.com/lovisto/
## Cuenta Admin Site
* admin/admin
## Cuentas usuarios
* Irene/1507julio
* Hugo/0510octubre
* Sara/0510octubre
## Resumen parte obligatoria
* Lo Visto, cumple con todos los aspectos que requería la parte obligatoria.
Sin estar logeado, en la página principal podemos ver las distintas aportaciones que se han realizado para los recursos
reconocidos Youtube, Wikipedia y Aemet. Para cada aportación se muestra la información que se pedía, así como, el número de comentarios
y votos que ha recibido. Además, también podemos visualizar la página de información del sitio y el listado de las aportaciones.
Una vez que nos logeamos, podemos acceder a ver el perfil del usuario, y podremos realizar nuevas aportaciones,añadir comentarios
y votar. En la página principal,  también veremos un listado de las últimas 5 aportaciones del usuario. Por último,podremos cambiar
de modo claro a modo oscuro y viceversa.
## Lista partes opcionales
* Inclusión de un favicon del sitio
* Páginas en formato XML y JSON
* Mejora del diseño del sitio con CSS y Bootstrap.
* Recursos no reconocidos