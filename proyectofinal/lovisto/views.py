from django.shortcuts import render, redirect
from django.contrib.auth import logout
from .forms import AportacionForm, ComentarioYForm, ComentarioAForm, ComentarioWForm, ComentarioRNForm
from .models import Usuario, Youtube, Wikipedia, Aemet, Aportacion, \
    Comentario_W, Comentario_A, Comentario_Y, \
    Votos_Y, Votos_W, Votos_A, RecursoN, Votos_RN, Comentario_RN
import json
import urllib.request
import urllib.parse
from .wikipedia_parser import Wikipedia_Channel
from .aemet_parser import Aemet_Channel
from bs4 import BeautifulSoup


def index(request):
    """Función principal, en la cual extremos toda la información que queremos de las aportaciones
    que realizan los usuarios.En primer lugar, recibimos un POST del formulario para la aportación,
    y analizamos el action del POST, si es Positivo o Negativo,nos encargamos de los votos para cada
     aportación, si el action es aportacion, tratamos de extaer la información."""

    form = AportacionForm(request.POST)
    if request.method == "POST":
        action = request.POST['action']
        if action == "Positivo" or action == "Negativo":
            if request.POST['youtub'] == "True":
                votos_Y(request)
            elif request.POST['wikipedi'] == "True":
                votos_W(request)
            elif request.POST['aeme'] == "True":
                votos_A(request)
            elif request.POST['recurn'] == "True":
                votos_RN(request)
        elif action == "aportacion":
            if form.is_valid():
                titulo = request.POST['titulo']
                url = request.POST['url']
                if not url.startswith('https://') and not url.startswith('http://'):
                    url = 'https://' + url
                usuario = Usuario.objects.get(usuario=request.user.username)
                contenido = Aportacion(titulo=titulo, url=url, usuario=usuario)
                # Tenemos tres recursos reconocidos, debemos conocer de cual se trata la aportación
                sitio = urllib.parse.unquote(url.split('/')[2])
                if sitio == 'www.youtube.com' or sitio == 'youtube.com':
                    try:
                        # Para youtube extraemos la información de un doc en formato JSON
                        patron = "https://www.youtube.com/oembed?format=json&url="
                        url_json = patron + url
                        with urllib.request.urlopen(url_json) as json_doc:
                            json_str = json_doc.read().decode(encoding="ISO-8859-1")
                            youtube_json = json.loads(json_str)
                        video_emb = urllib.parse.unquote(youtube_json["html"].split('"')[5])
                        # Guardamos toda la información extraida
                        y = Youtube(usuario=usuario, url=url, titulo=youtube_json["title"],
                                    autor=youtube_json["author_name"], contenido=video_emb)
                        contenido.save()
                        y.save()
                    except:
                        context = {'usuario': usuario}
                        return render(request,'noaporte.html', context)

                elif sitio == 'es.wikipedia.org':
                    '''Para cada articulo de wikipedia, extremos la imagen de un doc JSON 
                     y el contenido de un XML con wikipedia_parser'''
                    try:
                        articulo = urllib.parse.unquote((url.split('/')[4]))
                        url_json = 'https://es.wikipedia.org/w/api.php?action=query&titles=' + articulo \
                                   + '&prop=pageimages&format=json&pithumbsize=100'
                        with urllib.request.urlopen(url_json) as json_doc:
                            json_str = json_doc.read().decode(encoding="ISO-8859-1")
                            wikipedia_json = json.loads(json_str)
                            num = str(wikipedia_json["query"]["pages"])
                            num = urllib.parse.unquote(num.split("'")[1])
                        imagen = wikipedia_json["query"]["pages"][num]["thumbnail"]["source"]

                        url_xml = 'https://es.wikipedia.org/w/api.php?action=query&format=xml&titles=' + articulo \
                                  + '&prop=extracts&exintro&explaintext'
                        xmlStream = urllib.request.urlopen(url_xml)
                        channel = Wikipedia_Channel(xmlStream)
                        data = channel.articulo()[:400]
                        copy = "https://es.wikipedia.org/wiki/"+articulo
                        # Guardamos toda la información extraida
                        contenido.save()
                        w = Wikipedia(usuario=usuario, contenido=data, imagen=imagen, copy=copy, titulo=articulo)
                        w.save()
                    except:
                        context = {'usuario': usuario}
                        return render(request, 'noaporte.html', context)

                elif sitio == 'www.aemet.es' or sitio == 'aemet.es':
                    try:
                        # Debemos conocer el identificador del municipio
                        num = urllib.parse.unquote(url.split('-id')[1])
                        # Extraemos toda la info de un doc XML con aemet_parser
                        url_xml = 'https://www.aemet.es/xml/municipios/localidad_' + num + '.xml'
                        xmlStream = urllib.request.urlopen(url_xml)
                        channel = Aemet_Channel(xmlStream)
                        data = channel.prediccion()
                        municipio = data[0]['ciudad']
                        provincia = data[0]['provincia']
                        copy = data[0]['copyright']
                        prediccion = ""
                        for dias in data:
                            prediccion += "<li>" + dias['dia'] + "." + " Temperatura: " + dias['temp_max'] \
                                          + "/" + dias['temp_min'] + " Sensación: " + dias['sens_max'] \
                                          + "/" + dias['sens_min'] + "." + " Humedad relativa: " + dias['hum_max'] \
                                          + "/" + dias['hum_min'] + "</li>"
                        # Guardamos toda la información extraida
                        contenido.save()
                        a = Aemet(usuario=usuario, municipio=municipio, provincia=provincia,
                                  copy=copy, prediccion=prediccion, num=num)
                        a.save()
                    except:
                        context = {'usuario': usuario}
                        return render(request, 'noaporte.html', context)

                else:
                    try:
                        htmlStream = urllib.request.urlopen(url)
                        soup = BeautifulSoup(htmlStream, 'html.parser')
                        titulo = soup.find("meta", property="og:title", content=True)
                        titulo = titulo['content']
                        imagen = soup.find("meta", property="og:image", content=True)
                        imagen = imagen['content']
                        contenido.save()
                        n = RecursoN(usuario=usuario, titulo=titulo, imagen=imagen,)
                        n.save()
                    except:
                        context = {'usuario': usuario}
                        return render(request, 'noaporte.html', context)



                    
        elif action == "Modo oscuro":
            usuario = Usuario.objects.get(usuario=request.user.username)
            usuario.tema = "oscuro"
            usuario.save()
        elif action == "Modo claro":
            usuario = Usuario.objects.get(usuario=request.user.username)
            usuario.tema = "claro"
            usuario.save()

        return redirect('/lovisto/')

    else:
        # Si lo que recibimos es un GET, nos almacenamos en variables, toda la lista de aportaciones para cada recurso
        listado_contenidos = Youtube.objects.all()[:10]
        listado_contenidos_w = Wikipedia.objects.all()[:10]
        listado_contenidos_a = Aemet.objects.all()[:10]
        listado_recurso_no = RecursoN.objects.all()[:10]
        listado_votos_y = []
        listado_votos_w = []
        listado_votos_a = []
        listado_votos_rn = []
        usuario = ""
        cinco_aport =""
        if request.user.is_authenticated:
            # si el usuario esta autenticado, hacemos los mismo pero con los votos
            usuario = Usuario.objects.get(usuario=request.user.username)
            listado_votos_y = votosy(usuario.votos_y_set.all())
            listado_votos_w = votosw(usuario.votos_w_set.all())
            listado_votos_a = votosa(usuario.votos_a_set.all())
            listado_votos_rn = votosrn(usuario.votos_rn_set.all())
            cinco_aport = aportaciones(usuario.aportacion_set.all())

        context = {'form': form, 'listado_contenidos': listado_contenidos, 'listado_contenidos_w': listado_contenidos_w,
                   'listado_contenidos_a': listado_contenidos_a, 'listado_recurso_no': listado_recurso_no, 'votosyt': listado_votos_y,
                   'votoswk': listado_votos_w, 'votosam': listado_votos_a,'votosrn': listado_votos_rn, 'usuario': usuario,
                   'cinco_aport': cinco_aport}

    if request.GET.get('format') == 'xml':
        return render(request, 'doc_index.xml', context, content_type="text/xml")
    elif request.GET.get('format') == 'json':
        return render(request, 'doc_index.json', context, content_type="text/json")

    return render(request, 'index.html', context)


def votos_Y(request):
    """En esta función, tratamos los votos para las aportaciones de Youtube. En primer lugar, debemos conocer de que
    video se trata y que usuario ha votado. En caso de vota Positivo, ponemos el valor de voto_positivo a 1 y el
    voto_negativo a 0 y guardamos los valores. Si por el contrario,el voto del usuario es Negativo, ponermos el
    voto_negativo a 1 y el positivo a 0. Tras ello, actualizamos los contadores."""
    try:
        id = request.POST['youtube']
        aporte = Youtube.objects.get(id=id)
        usuario = Usuario.objects.get(usuario=request.user.username)
        if request.POST['action'] == 'Positivo':
            try:
                v = Votos_Y.objects.get(aporte=aporte, usuario=usuario)
                v.voto_positivo = 1
                v.voto_negativo = 0
                v.save()
                aporte.cont_pos += 1
                aporte.cont_neg -= 1
                aporte.save()
            except:
                v = Votos_Y(aporte=aporte, usuario=usuario, voto_positivo=1)
                v.save()
                aporte.cont_pos += 1
                aporte.save()
        elif request.POST['action'] == 'Negativo':
            try:
                v = Votos_Y.objects.get(aporte=aporte, usuario=usuario)
                v.voto_positivo = 0
                v.voto_negativo = 1
                v.save()
                aporte.cont_pos -= 1
                aporte.cont_neg += 1
                aporte.save()
            except:
                v = Votos_Y(aporte=aporte, usuario=usuario, voto_negativo=1)
                v.save()
                aporte.cont_neg += 1
                aporte.save()
    except Youtube.DoesNotExist:
        return render(request, 'error.html')


def votosy(videos):
    # Ahora tratamos de generar una lista con los videos que han sido votados
    votados = []
    for voto in videos:
        if voto.voto_positivo == 1 or voto.voto_negativo == 1:
            votados.append(voto.aporte)
    return votados


def votos_W(request):
    try:
        id = request.POST['wikipedia']
        aporte = Wikipedia.objects.get(id=id)
        usuario = Usuario.objects.get(usuario=request.user.username)
        if request.POST['action'] == 'Positivo':
            try:
                v = Votos_W.objects.get(aporte=aporte, usuario=usuario)
                v.voto_positivo = 1
                v.voto_negativo = 0
                v.save()
                aporte.cont_pos += 1
                aporte.cont_neg -= 1
                aporte.save()
            except:
                v = Votos_W(aporte=aporte, usuario=usuario, voto_positivo=1)
                v.save()
                aporte.cont_pos += 1
                aporte.save()
        elif request.POST['action'] == 'Negativo':
            try:
                v = Votos_W.objects.get(aporte=aporte, usuario=usuario)
                v.voto_positivo = 0
                v.voto_negativo = 1
                v.save()
                aporte.cont_pos -= 1
                aporte.cont_neg += 1
                aporte.save()
            except:
                v = Votos_W(aporte=aporte, usuario=usuario, voto_negativo=1)
                v.save()
                aporte.cont_neg += 1
                aporte.save()
    except Wikipedia.DoesNotExist:
        return render(request, 'Error.html')


def votosw(articulos):
    votados = []
    for voto in articulos:
        if voto.voto_positivo == 1 or voto.voto_negativo == 1:
            votados.append(voto.aporte)
    return votados


def votos_A(request):
    try:
        id = request.POST['aemet']
        aporte = Aemet.objects.get(id=id)
        usuario = Usuario.objects.get(usuario=request.user.username)
        if request.POST['action'] == 'Positivo':
            try:
                v = Votos_A.objects.get(aporte=aporte, usuario=usuario)
                v.voto_positivo = 1
                v.voto_negativo = 0
                v.save()
                aporte.cont_pos += 1
                aporte.cont_neg -= 1
                aporte.save()
            except:
                v = Votos_A(aporte=aporte, usuario=usuario, voto_positivo=1)
                v.save()
                aporte.cont_pos += 1
                aporte.save()
        elif request.POST['action'] == 'Negativo':
            try:
                v = Votos_A.objects.get(aporte=aporte, usuario=usuario)
                v.voto_positivo = 0
                v.voto_negativo = 1
                v.save()
                aporte.cont_pos -= 1
                aporte.cont_neg += 1
                aporte.save()
            except:
                v = Votos_A(aporte=aporte, usuario=usuario, voto_negativo=1)
                v.save()
                aporte.cont_neg += 1
                aporte.save()
    except Aemet.DoesNotExist:
        return render(request, 'Error.html')


def votosa(tiempo):
    votados = []
    for voto in tiempo:
        if voto.voto_positivo == 1 or voto.voto_negativo == 1:
            votados.append(voto.aporte)
    return votados


def votos_RN(request):
    try:
        id = request.POST['recurson']
        aporte = RecursoN.objects.get(id=id)
        usuario = Usuario.objects.get(usuario=request.user.username)
        if request.POST['action'] == 'Positivo':
            try:
                v = Votos_RN.objects.get(aporte=aporte, usuario=usuario)
                v.voto_positivo = 1
                v.voto_negativo = 0
                v.save()
                aporte.cont_pos += 1
                aporte.cont_neg -= 1
                aporte.save()
            except:
                v = Votos_RN(aporte=aporte, usuario=usuario, voto_positivo=1)
                v.save()
                aporte.cont_pos += 1
                aporte.save()
        elif request.POST['action'] == 'Negativo':
            try:
                v = Votos_RN.objects.get(aporte=aporte, usuario=usuario)
                v.voto_positivo = 0
                v.voto_negativo = 1
                v.save()
                aporte.cont_pos -= 1
                aporte.cont_neg += 1
                aporte.save()
            except:
                v = Votos_RN(aporte=aporte, usuario=usuario, voto_negativo=1)
                v.save()
                aporte.cont_neg += 1
                aporte.save()
    except RecursoN.DoesNotExist:
        return render(request, 'Error.html')


def votosrn(recursos):
    votados = []
    for voto in recursos:
        if voto.voto_positivo == 1 or voto.voto_negativo == 1:
            votados.append(voto.aporte)
    return votados


def pag_aportaciones(request):
    aportaciones = Aportacion.objects.all()
    aportaciones = reversed(aportaciones) # Queremos un listado que comience con las aportaciones más recientes
    if request.user.is_authenticated:
        usuario = Usuario.objects.get(usuario=request.user.username)
        context = {'aportaciones': aportaciones, 'usuario': usuario}
    else:
        context = {'aportaciones': aportaciones}

    if request.GET.get('format') == 'xml':
        return render(request, 'doc_xml.xml', context, content_type="text/xml")
    elif request.GET.get('format') == 'json':
        return render(request, 'doc_JSON.json', context, content_type="text/json")

    return render(request, 'aportaciones.html', context)


def aportacion(request, llave, id):
    youtube = ""
    wikipedia = ""
    aemet = ""
    recurson=""
    context = {}
    try:
        youtube = Youtube.objects.get(titulo=llave, id=id)
        form = ComentarioYForm(request.POST)
    except Youtube.DoesNotExist:
        try:
            wikipedia = Wikipedia.objects.get(titulo=llave, id=id)
            form = ComentarioWForm(request.POST)
        except Wikipedia.DoesNotExist:
            try:
                aemet = Aemet.objects.get(municipio=llave, id=id)
                form = ComentarioAForm(request.POST)
            except Aemet.DoesNotExist:
                try:
                    recurson = RecursoN.objects.get(titulo=llave, id=id)
                    form = ComentarioRNForm(request.POST)
                except:
                    return render(request, 'Error.html')

    if request.method == "GET":
        usuario = Usuario.objects.get(usuario=request.user.username)
        if request.user.is_authenticated:
            if youtube:
                context = {'youtube': youtube, 'form': form, 'usuario': usuario}
            elif wikipedia:
                context = {'wikipedia': wikipedia, 'form': form, 'usuario': usuario}
            elif aemet:
                context = {'aemet': aemet, 'form': form, 'usuario': usuario}
            elif recurson:
                context = {'recurson': recurson, 'form': form, 'usuario': usuario}
        else:
            if youtube:
                context = {'youtube': youtube, 'form': form}
            elif wikipedia:
                context = {'wikipedia': wikipedia, 'form': form}
            elif aemet:
                context = {'aemet': aemet, 'form': form}
            elif recurson:
                context = {'recurson': recurson, 'form': form}
        return render(request, 'aportacion.html', context)
    else:  # Si el método es POST
        if form.is_valid():
            if youtube:
                aport_video = Youtube.objects.get(id=id)
                usuario = Usuario.objects.get(usuario=request.user.username)
                comentario = request.POST['comentario']
                # Guardamos el comentario
                c = Comentario_Y(aport_video=aport_video, usuario=usuario, comentario=comentario)
                c.save()
                aport_video.contador_c += 1
                aport_video.save()

            elif wikipedia:
                aport_articulo = Wikipedia.objects.get(id=id)
                usuario = Usuario.objects.get(usuario=request.user.username)
                comentario = request.POST['comentario']
                c = Comentario_W(aport_articulo=aport_articulo, usuario=usuario, comentario=comentario)
                c.save()
                # Contador para comentarios
                aport_articulo.contador_c += 1
                aport_articulo.save()
            elif aemet:
                aport_prediccion = Aemet.objects.get(id=id)
                usuario = Usuario.objects.get(usuario=request.user.username)
                comentario = request.POST['comentario']
                c = Comentario_A(aport_prediccion=aport_prediccion, usuario=usuario, comentario=comentario)
                c.save()
                aport_prediccion.contador_c += 1
                aport_prediccion.save()
            elif recurson:
                aport = RecursoN.objects.get(id=id)
                usuario = Usuario.objects.get(usuario=request.user.username)
                comentario = request.POST['comentario']
                c = Comentario_RN(aport=aport, usuario=usuario, comentario=comentario)
                c.save()
                aport.contador_c += 1
                aport.save()

    return redirect(request.path)


def aportaciones(aportes_usuario):
    cinco_aport = []
    # Nos quedamos con las cinco aportaciones más recientes del usuario
    for aport in aportes_usuario[::-1][:5]:
        cinco_aport.append(aport)
    return cinco_aport


def pag_usuario(request):
    if request.user.is_authenticated:
        usuario = Usuario.objects.get(usuario=request.user.username)
        # Solo queremos que se muestren en el listado general las últimas 5 aportaciones del usuario
        cinco_aport = aportaciones(usuario.aportacion_set.all())
        context = {'usuario': usuario, 'cinco_aport': cinco_aport}
        if request.GET.get('format') == 'xml':
            return render(request, 'doc_usuario.xml', context, content_type="text/xml")
        elif request.GET.get('format') == 'json':
            return render(request, 'doc_usuario.json', context, content_type="text/json")
        return render(request, 'usuario.html', context)
    else:
        return render(request, 'error.html')


def logout_view(request):
    logout(request)
    return redirect('/lovisto/')


def informacion(request):
    if request.user.is_authenticated:
        usuario = Usuario.objects.get(usuario=request.user.username)
        context = {'usuario': usuario}
    else:
        context = {}

    if request.GET.get('format') == 'xml':
        return render(request, 'doc_info.xml', context, content_type="text/xml")
    elif request.GET.get('format') == 'json':
        return render(request, 'doc_info.json', context, content_type="text/json")
    return render(request, 'informacion.html', context)


def error(request,recurso):
    if request.user.is_authenticated:
        usuario = Usuario.objects.get(usuario=request.user.username)
        context = {'usuario': usuario,'recurso': recurso}
    else:
        context = {'recurso': recurso}
    return render(request,'error.html', context)