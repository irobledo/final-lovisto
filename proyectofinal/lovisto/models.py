from django.db import models
# Create your models here.


class Usuario(models.Model):
    usuario = models.CharField(max_length=20)
    tema = models.CharField(max_length=15, default="claro")

    def __str__(self):
        return self.usuario

class RecursoN(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=30)
    imagen = models.ImageField(default='null')
    cont_pos = models.IntegerField(default=0)
    cont_neg = models.IntegerField(default=0)
    contador_c = models.IntegerField(default=0)

    def __str__(self):
        return self.titulo

class Comentario_RN(models.Model):
    aport = models.ForeignKey(RecursoN, on_delete=models.CASCADE)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    comentario = models.TextField()
    fecha = models.DateTimeField(auto_now_add=True)

class Votos_RN(models.Model):
    aporte = models.ForeignKey(RecursoN, on_delete=models.CASCADE)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    voto_positivo = models.IntegerField(default='0')
    voto_negativo = models.IntegerField(default='0')


class Aemet(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    municipio = models.CharField(max_length=20)
    provincia = models.CharField(max_length=20)
    prediccion = models.TextField()
    copy = models.CharField(max_length=50)
    num = models.IntegerField()
    cont_pos = models.IntegerField(default=0)
    cont_neg = models.IntegerField(default=0)
    contador_c = models.IntegerField(default=0)

    def __str__(self):
        return self.municipio


class Aportacion(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    url = models.CharField(max_length=500)
    titulo = models.CharField(max_length=150)


class Youtube(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    url = models.CharField(max_length=500)
    titulo = models.CharField(max_length=150)
    autor = models.CharField(max_length=150, default='null')
    contenido = models.TextField()
    cont_pos = models.IntegerField(default=0)
    cont_neg = models.IntegerField(default=0)
    contador_c = models.IntegerField(default=0)

    def __str__(self):
        return self.titulo


class Wikipedia(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    titulo = models.TextField()
    contenido = models.TextField()
    imagen = models.ImageField(default='null')
    copy = models.CharField(max_length=150, default='null')
    cont_pos = models.IntegerField(default=0)
    cont_neg = models.IntegerField(default=0)
    contador_c = models.IntegerField(default=0)

    def __str__(self):
        return self.titulo


class Comentario_Y(models.Model):
    aport_video = models.ForeignKey(Youtube, on_delete=models.CASCADE)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    comentario = models.TextField()
    fecha = models.DateTimeField(auto_now_add=True)


class Comentario_W(models.Model):
    aport_articulo = models.ForeignKey(Wikipedia, on_delete=models.CASCADE)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    comentario = models.TextField()
    fecha = models.DateTimeField(auto_now_add=True)


class Comentario_A(models.Model):
    aport_prediccion = models.ForeignKey(Aemet, on_delete=models.CASCADE)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    comentario = models.TextField()
    fecha = models.DateTimeField(auto_now_add=True)


class Votos_Y(models.Model):
    aporte = models.ForeignKey(Youtube, on_delete=models.CASCADE)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    voto_positivo = models.IntegerField(default='0')
    voto_negativo = models.IntegerField(default='0')


class Votos_W(models.Model):
    aporte = models.ForeignKey(Wikipedia, on_delete=models.CASCADE)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    voto_positivo = models.IntegerField(default='0')
    voto_negativo = models.IntegerField(default='0')


class Votos_A(models.Model):
    aporte = models.ForeignKey(Aemet, on_delete=models.CASCADE)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    voto_positivo = models.IntegerField(default='0')
    voto_negativo = models.IntegerField(default='0')