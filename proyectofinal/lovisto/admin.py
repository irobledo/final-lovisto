from django.contrib import admin
from .models import Youtube, Aemet, Wikipedia,Aportacion, Comentario_Y, Comentario_A, \
    Comentario_W, Usuario, Votos_A, Votos_W, Votos_Y, RecursoN, Comentario_RN,Votos_RN
# Register your models here.
admin.site.register(Youtube)
admin.site.register(Aemet)
admin.site.register(Wikipedia)
admin.site.register(Aportacion)
admin.site.register(Comentario_A)
admin.site.register(Comentario_Y)
admin.site.register(Comentario_W)
admin.site.register(Usuario)
admin.site.register(Votos_Y)
admin.site.register(Votos_W)
admin.site.register(Votos_A)
admin.site.register(RecursoN)
admin.site.register(Comentario_RN)
admin.site.register(Votos_RN)
# De esta forma podemos gestionar los objectos de Recurso desde la interfaz de administrador
# Nombre de user: admin
# Contraseña: admin