# Generated by Django 3.1.7 on 2021-05-31 07:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Aemet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('municipio', models.CharField(max_length=20)),
                ('provincia', models.CharField(max_length=20)),
                ('prediccion', models.TextField()),
                ('copy', models.CharField(max_length=50)),
                ('num', models.IntegerField()),
                ('cont_pos', models.IntegerField(default=0)),
                ('cont_neg', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('usuario', models.CharField(max_length=20)),
                ('tema', models.CharField(default='claro', max_length=15)),
            ],
        ),
        migrations.CreateModel(
            name='Youtube',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.CharField(max_length=500)),
                ('titulo', models.CharField(max_length=150)),
                ('autor', models.CharField(default='null', max_length=150)),
                ('contenido', models.TextField()),
                ('cont_pos', models.IntegerField(default=0)),
                ('cont_neg', models.IntegerField(default=0)),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.usuario')),
            ],
        ),
        migrations.CreateModel(
            name='Wikipedia',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.TextField()),
                ('contenido', models.TextField()),
                ('imagen', models.ImageField(default='null', upload_to='')),
                ('copy', models.CharField(default='null', max_length=150)),
                ('cont_pos', models.IntegerField(default=0)),
                ('cont_neg', models.IntegerField(default=0)),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.usuario')),
            ],
        ),
        migrations.CreateModel(
            name='Votos_Y',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('voto_positivo', models.IntegerField(default='0')),
                ('voto_negativo', models.IntegerField(default='0')),
                ('aporte', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.youtube')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.usuario')),
            ],
        ),
        migrations.CreateModel(
            name='Votos_W',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('voto_positivo', models.IntegerField(default='0')),
                ('voto_negativo', models.IntegerField(default='0')),
                ('aporte', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.wikipedia')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.usuario')),
            ],
        ),
        migrations.CreateModel(
            name='Votos_A',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('voto_positivo', models.IntegerField(default='0')),
                ('voto_negativo', models.IntegerField(default='0')),
                ('aporte', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.aemet')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.usuario')),
            ],
        ),
        migrations.CreateModel(
            name='Comentario_Y',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comentario', models.TextField()),
                ('fecha', models.DateTimeField(auto_now_add=True)),
                ('aport_video', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.youtube')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.usuario')),
            ],
        ),
        migrations.CreateModel(
            name='Comentario_W',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comentario', models.TextField()),
                ('fecha', models.DateTimeField(auto_now_add=True)),
                ('aport_video', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.wikipedia')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.usuario')),
            ],
        ),
        migrations.CreateModel(
            name='Comentario_A',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comentario', models.TextField()),
                ('fecha', models.DateTimeField(auto_now_add=True)),
                ('aport_video', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.aemet')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.usuario')),
            ],
        ),
        migrations.CreateModel(
            name='Aportacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.CharField(max_length=500)),
                ('titulo', models.CharField(max_length=150)),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.usuario')),
            ],
        ),
        migrations.AddField(
            model_name='aemet',
            name='usuario',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.usuario'),
        ),
    ]
