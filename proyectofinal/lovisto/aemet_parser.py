from xml.sax.handler import ContentHandler
from xml.sax import make_parser


class Aemet_Handler(ContentHandler):

    def __init__(self):
        self.content = ""
        self.inContent = False
        self.inDia = False
        self.dia = ""
        self.inTemperatura = False
        self.temp_max = ""
        self.temp_min = ""
        self.inSens = False
        self.sens_max = ""
        self.sens_min = ""
        self.inHumedad = False
        self.hum_max = ""
        self.hum_min = ""
        self.prediccion = []
        self.copy = ""
        self.provincia = ""

    def startElement(self, name, attrs):
        """Cuando encontremos el todos estos elementos ponemos inContent a true,
        pues su contenido es lo que necesito extraer """
        if name == 'copyright':
            self.inContent = True
        elif name == 'nombre':
            self.inContent = True
        elif name == 'provincia':
            self.inContent = True
        elif name == 'dia':
            self.dia = attrs.get('fecha')
            self.inDia = True
        elif self.inDia:
            if name == 'temperatura':
                self.inTemperatura = True
            elif name == 'sens_termica':
                self.inSens = True
            elif name == 'humedad_relativa':
                self.inHumedad = True
            elif self.inTemperatura:
                if name == 'maxima':
                    self.inContent = True
                elif name == 'minima':
                    self.inContent = True
            elif self.inSens:
                if name == 'maxima':
                    self.inContent = True
                elif name == 'minima':
                    self.inContent = True
            elif self.inHumedad:
                if name == 'maxima':
                    self.inContent = True
                elif name == 'minima':
                    self.inContent = True

    def endElement(self, name):
        """Una vez que reconocemos el final de cada elemento,,
        extraemos su contenido y tras ello, ponemos inContent a False.
        En predicción iremos añadiendo todos los elementos"""
        global prediccion

        if name == 'copyright':
            self.copy = self.content
            self.content = ""
            self.inContent = False
        elif name == 'nombre':
            self.ciudad = self.content
            self.content = ""
            self.inContent = False
        elif name == 'provincia':
            self.provincia = self.content
            self.content = ""
            self.inContent = False
        elif name == 'dia':
            self.inDia = False
            self.prediccion.append({'copyright': self.copy,
                                     'ciudad': self.ciudad,
                                     'provincia': self.provincia,
                                     'dia': self.dia,
                                     'temp_max': self.temp_max,
                                     'temp_min': self.temp_min,
                                     'sens_max': self.sens_max,
                                     'sens_min': self.sens_min,
                                     'hum_max': self.hum_max,
                                     'hum_min': self.hum_min})
        elif self.inDia:
            if name == 'temperatura':
                self.inTemperatura = False
            elif name == 'sens_termica':
                self.inSens = False
            elif name == 'humedad_relativa':
                self.inHumedad = False
            elif self.inTemperatura:
                if name == 'maxima':
                    self.temp_max = self.content
                    self.content = ""
                    self.inContent = False
                elif name == 'minima':
                    self.temp_min = self.content
                    self.content = ""
                    self.inContent = False
            elif self.inSens:
                if name == 'maxima':
                    self.sens_max = self.content
                    self.content = ""
                    self.inContent = False
                elif name == 'minima':
                    self.sens_min = self.content
                    self.content = ""
                    self.inContent = False
            elif self.inHumedad:
                if name == 'maxima':
                    self.hum_max = self.content
                    self.content = ""
                    self.inContent = False
                elif name == 'minima':
                    self.hum_min = self.content
                    self.content = ""
                    self.inContent = False

    def characters(self, chars):
        if self.inContent:
            # Contenido que me interesaba extraer
            self.content = self.content + chars


class Aemet_Channel:

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = Aemet_Handler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def prediccion(self):
        return self.handler.prediccion
