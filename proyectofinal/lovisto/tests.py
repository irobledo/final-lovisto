from django.test import TestCase
from django.test import Client
# Create your tests here.

class GetTest(TestCase):

    def test_logout(self):
        respuesta = self.client.get('/lovisto/logout')
        self.assertEqual(respuesta.status_code, 302)

    def test_login(self):
        respuesta = self.client.get('/login')
        self.assertEqual(respuesta.status_code, 200)

    def test_principal(self):
        respuesta = self.client.get('/lovisto/')
        self.assertEqual(respuesta.status_code, 200)

    def test_info(self):
        c = Client()
        response = c.get('/lovisto/informacion')
        content = response.content.decode('utf-8')
        self.assertIn('Irene Robledo Calero', content)

    def test_pag_info(self):
        respuesta = self.client.get('/lovisto/informacion')
        self.assertEqual(respuesta.status_code, 200)

    def test_xml(self):
        c = Client()
        response = c.get('/lovisto/aportaciones?format=xml')
        content = response.content.decode('utf-8')
        self.assertIn('<?xml version="1.0" ?>', content)


    def test_xml_info(self):
        c = Client()
        response = c.get('/lovisto/informacion?format=xml')
        content = response.content.decode('utf-8')
        self.assertIn('<autoria>', content)

    def test_xml_i(self):
        c = Client()
        response = c.get('/lovisto/?format=xml')
        content = response.content.decode('utf-8')
        self.assertIn('<titulo>', content)


    def test_json(self):
        c = Client()
        response = c.get('/lovisto/aportaciones?format=json')
        content = response.content.decode('utf-8')
        self.assertIn("Aportaciones", content)

    def test_json_i(self):
        c = Client()
        response = c.get('/lovisto/?format=json')
        content = response.content.decode('utf-8')
        self.assertIn("Titulo", content)

    def test_json_info(self):
        c = Client()
        response = c.get('/lovisto/informacion?format=json')
        content = response.content.decode('utf-8')
        self.assertIn("autoria", content)

    def test_pag_user(self):
        respuesta = self.client.get('/lovisto/pag_usuario')
        self.assertEqual(respuesta.status_code, 200)

class PostTest(TestCase):

    def test_login(self):
        respuesta = self.client.post('/login')
        self.assertEqual(respuesta.status_code, 200)



