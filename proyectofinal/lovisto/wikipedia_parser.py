from xml.sax.handler import ContentHandler
from xml.sax import make_parser


class Wikipedia_Handler(ContentHandler):

    def __init__(self):
        self.inContent = False
        self.content = ""
        self.texto = ""
        self.articulo = "" #Inicializamos los parámetros que me interesan

    def startElement(self, name, attrs):
        '''Cuando encontremos el elemento extract ponemos inContent a true,
        pues su contenido es lo que necesito '''
        if name == 'extract':
            self.inContent = True

    def endElement(self, name):
        '''Una vez que reconocemos el final del elemento extract,
        extraemos su contenido y tras ello, ponemos inContent a False'''
        global articulo

        if name == 'extract':
            self.texto = self.content
            self.content = ""
            self.inContent = False
            self.articulo = self.texto

    def characters(self, chars):
        if self.inContent:
            self.content = self.content + chars #Contenido que me interesaba extraer

class Wikipedia_Channel:

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = Wikipedia_Handler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def articulo (self):
        return self.handler.articulo
