from django import forms
from .models import  Aportacion, Comentario_Y,Comentario_A,Comentario_W, Comentario_RN

class AportacionForm(forms.ModelForm):
    class Meta:
        model = Aportacion
        fields = ('titulo', 'url',)

class ComentarioRNForm(forms.ModelForm):
    class Meta:
        model = Comentario_RN
        fields = ('comentario',)


class ComentarioYForm(forms.ModelForm):
    class Meta:
        model = Comentario_Y
        fields = ('comentario',)

class ComentarioWForm(forms.ModelForm):
    class Meta:
        model = Comentario_W
        fields = ('comentario',)

class ComentarioAForm(forms.ModelForm):
    class Meta:
        model = Comentario_A
        fields = ('comentario',)