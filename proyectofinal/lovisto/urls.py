from django.urls import path
from lovisto import views

urlpatterns = [
    path('', views.index),
    path('aportaciones', views.pag_aportaciones),
    path('logout', views.logout_view, name='logout'),
    path('informacion', views.informacion, name='informacion'),
    path('<str:llave>-<str:id>', views.aportacion),
    path('pag_usuario', views.pag_usuario),
    path('<str:recurso>', views.error),


]
